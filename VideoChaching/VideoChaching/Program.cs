﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoChaching.Model;

namespace VideoChaching
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputFile = ConfigurationManager.AppSettings["Path"];
            ParseData inputData = new ParseData(inputFile);

            List<CacheServer> cacheServers = new List<CacheServer>();
            for (int i = 0; i < inputData.Data.CacheServerCount; i++)
            {
                cacheServers.Add(new CacheServer() {CacheServerId = i, Size = inputData.Data.CacheSize});
            }
            List<Video> videos = inputData.GetVideo(inputData.Data.VideoCount);
            List<Endpoint> endpoints = inputData.GetEndpoint(inputData.Data.EndpointCount, cacheServers);
            List<RequestDescription> requestDescriptions =
                inputData.GetRequestDescriptions(inputData.Data.RequestDescriptions);
            inputData.CreateToDictionatyVideoEndpoint(requestDescriptions, endpoints, videos);
            inputData.ProcessingData(requestDescriptions, endpoints, videos);
            inputData.Output(cacheServers);
        }
    }
}

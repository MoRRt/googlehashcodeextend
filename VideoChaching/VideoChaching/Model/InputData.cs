﻿namespace VideoChaching.Model
{
    public class InputData
    {
        public int VideoCount { get; set; }
        public int EndpointCount { get; set; }
        public int RequestDescriptions { get; set; }
        public int CacheServerCount { get; set; }
        public int CacheSize { get; set; }
    }
}
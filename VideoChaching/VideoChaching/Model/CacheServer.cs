﻿using System.Collections.Generic;

namespace VideoChaching.Model
{
    public class CacheServer
    {
        public int CacheServerId { get; set; }
        public int Size { get; set; }
        public int CurrentSize { get; set; }
        public List<Video> VideoList { get; set; }

        public CacheServer()
        {
            VideoList = new List<Video>();
        }
    }
}
﻿namespace VideoChaching.Model
{
    public class RequestDescription
    {
        public int EndpointId { get; set; } 
        public int Request { get; set; }
        public int VideoId { get; set; }
    }
}
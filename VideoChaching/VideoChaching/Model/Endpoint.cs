﻿using System.Collections.Generic;

namespace VideoChaching.Model
{
    public class Endpoint
    {
        public int EndpointId { get; set; }
        public int LatencyDataCenter { get; set; }
        public Dictionary<CacheServer, int> DictionaryCacheServer { get; set; }
        public Dictionary<Video, int> DictionaryVideoEndpoint { get; set; }

        public Endpoint()
        {
            DictionaryCacheServer = new Dictionary<CacheServer, int>();
            DictionaryVideoEndpoint = new Dictionary<Video, int>();
        }
    }
}
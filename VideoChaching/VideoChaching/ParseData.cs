﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using VideoChaching.Model;

namespace VideoChaching
{
    public class ParseData
    {
        public InputData Data { get; set; }
        private readonly string[] lines;
        public ParseData(string path)
        {
            Data = new InputData();
            lines = File.ReadAllLines(path);
            var infos = lines.First().Split(' ').Select(int.Parse).ToArray();
            Data.VideoCount = infos[0];
            Data.EndpointCount = infos[1];
            Data.RequestDescriptions = infos[2];
            Data.CacheServerCount = infos[3];
            Data.CacheSize = infos[4];
         
        }

        public List<Video> GetVideo(int countVideo)
        {
            var videos = new List<Video>();
            var infoVideo = lines[1].Split(' ').Select(int.Parse).ToArray();
            for (int i = 0; i < countVideo; i++)
            {
                videos.Add(new Video() { VideoId = i, Size = infoVideo[i] });
            }
            return videos;
        }

        public List<Endpoint> GetEndpoint(int countEndpoint, List<CacheServer> cacheServers)
        {
            var endpoints = new List<Endpoint>();
            int n = 2;
            for (int i = 0; i < countEndpoint; i++)
            {
                var infos = lines[n].Split(' ').Select(int.Parse).ToArray();
                Endpoint endpoint = new Endpoint { EndpointId = i, LatencyDataCenter = infos[0]};
                for (int j = 1; j <= infos[1]; j++)
                {
                    var inf = lines[n + j].Split(' ').Select(int.Parse).ToArray();
                    endpoint.DictionaryCacheServer.Add(cacheServers.Find(c => c.CacheServerId == inf[0]),inf[1]);
                }
                n = n + infos[1] + 1;
                endpoints.Add(endpoint);
            }
            return endpoints;
        }

        public List<RequestDescription> GetRequestDescriptions(int countDescriptions)
        {
            var requestDescriptions = new List<RequestDescription>();
            for (int i = 0; i < countDescriptions; i++)
            {
                var info = lines[lines.Length - i - 1].Split(' ').Select(int.Parse).ToArray();
                requestDescriptions.Add(new RequestDescription() { VideoId = info[0], EndpointId = info[1], Request = info[2] });
            }
            return requestDescriptions;
        }

        public void CreateToDictionatyVideoEndpoint(List<RequestDescription> requestDescriptions, List<Endpoint> endpoints, List<Video> videos)
        {
            foreach (var description in requestDescriptions)
            {
                Endpoint endPoint = endpoints.Find(e => e.EndpointId == description.EndpointId);
                endPoint.DictionaryVideoEndpoint.Add(videos.Find(v => v.VideoId == description.VideoId),
                    description.Request);

            }
        }

        public Dictionary<RequestDescription, int> ProcessingData(List<RequestDescription> requestDescriptions, List<Endpoint> endpoints,
            List<Video> videos)
        {
            Dictionary<RequestDescription,int> savingTime = new Dictionary<RequestDescription,int>();
            foreach (var endpoint in endpoints)
            {
                //cохранять данные о запросе и сохраненном времени в Dictionary<RequestDescription, int>
                //добавлть валидные видео в CacheServer.VideoList<Video>
                var keyValuePairs = endpoint.DictionaryCacheServer.OrderBy(pair => pair.Value);
                bool isOut = false;

                while (!isOut)
                {
                    int count = endpoint.DictionaryVideoEndpoint.Count;

                    IOrderedEnumerable<KeyValuePair<Video, int>> orderedEnumerable = endpoint.DictionaryVideoEndpoint.OrderBy(pair => pair.Value);
                    foreach (KeyValuePair<Video, int> videoRequest in orderedEnumerable)
                    {
                        foreach (var cacheServerInfo in keyValuePairs)
                        {
                            if (videoRequest.Key.Size <= cacheServerInfo.Key.Size - cacheServerInfo.Key.CurrentSize)
                            {
                                cacheServerInfo.Key.VideoList.Add(videoRequest.Key);
                                cacheServerInfo.Key.CurrentSize = cacheServerInfo.Key.CurrentSize + videoRequest.Key.Size;
                                savingTime.Add(
                                    new RequestDescription
                                    {
                                        EndpointId = endpoint.EndpointId,
                                        Request = videoRequest.Value,
                                        VideoId = videoRequest.Key.VideoId
                                    },
                                    endpoint.LatencyDataCenter - cacheServerInfo.Value);
                                endpoint.DictionaryVideoEndpoint.Remove(videoRequest.Key);
                            }
                        }
                    }
                    isOut = endpoint.DictionaryVideoEndpoint.Count == count;
                }
            }
            return savingTime;
        }

        public void Output(List<CacheServer> cacheServers)
        {
            var outputFile = ConfigurationManager.AppSettings["Path2"];
            using (var writer = new StreamWriter(outputFile, true))
            {
                writer.WriteLine(cacheServers.Count);

            }
            foreach (var server in cacheServers)
            {
                using (var writer = new StreamWriter(outputFile, true))
                {
                    writer.WriteLine(server.CacheServerId + " ");

                    foreach (var video in server.VideoList)
                    {
                        writer.Write(video.VideoId + " ");
                    }
                }
            }
        }


    }
}